package test

import (
	"testing"

	. "gitlab.com/alexmalard/shortest-path-project/solution"
	"gotest.tools/assert"
)

// Simple test element implementing PriorityQueueElement interface contract
type MyTestPQElement struct {
	id    int
	value float64
	index int
}

func MakeMyTestPQElement(id int, value float64) *MyTestPQElement {
	return &MyTestPQElement{id, value, 0}
}

func (n *MyTestPQElement) GetId() int {
	return n.id
}

func (n *MyTestPQElement) GetIndex() int {
	return n.index
}

func (n *MyTestPQElement) SetIndex(index int) {
	n.index = index
}

func (n *MyTestPQElement) Compare(other PriorityQueueElement) int {
	otherNode := other.(*MyTestPQElement)
	if n.value > otherNode.value {
		return 1
	}
	if n.value == otherNode.value {
		return 0
	}
	return -1
}

// Test method checking PriorityQueue behaviour
func TestPQ(t *testing.T) {
	pq := MakePriorityQueue()
	pq.Push(MakeMyTestPQElement(1, 3.2))
	pq.Push(MakeMyTestPQElement(2, 4.5))
	pq.Push(MakeMyTestPQElement(3, 0.5))
	pq.Push(MakeMyTestPQElement(4, 9.9))
	element, _ := pq.Pop()
	myElement := element.(*MyTestPQElement)
	assert.Equal(t, myElement.value, 0.5)
	pq.Push(MakeMyTestPQElement(3, 5.1))
	element, _ = pq.Pop()
	myElement = element.(*MyTestPQElement)
	assert.Equal(t, myElement.value, 3.2)
	pq.Push(MakeMyTestPQElement(4, 0.7))
	element, _ = pq.Pop()
	myElement = element.(*MyTestPQElement)
	assert.Equal(t, myElement.value, 0.7)
	pq.Push(MakeMyTestPQElement(4, 1.1))
	pq.Push(MakeMyTestPQElement(4, 6.3))
	element, _ = pq.Pop()
	myElement = element.(*MyTestPQElement)
	assert.Equal(t, myElement.value, 4.5)
}
