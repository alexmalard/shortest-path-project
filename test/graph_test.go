package test

import (
	"fmt"
	"testing"

	. "gitlab.com/alexmalard/shortest-path-project/solution"

	"gotest.tools/assert"
)

// Test simple graph construction
func TestBuildGraph(t *testing.T) {
	g := BuildSimpleGraph()
	node0, _ := g.GetNode(0)
	edge13, _ := g.GetEdgeFromId(13)
	assert.Equal(t, len(node0.GetOutgoingEdges()), 2)
	assert.Equal(t, len(g.GetNodes()), 8)
	assert.Equal(t, len(g.GetEdges()), 15)
	assert.Equal(t, edge13.GetName(), "6>0")
}

// Test Dijkstra's algorithm on simple graph
func TestDijsktra(t *testing.T) {
	g := BuildSimpleGraph()
	node0, _ := g.GetNode(0)
	node6, _ := g.GetNode(6)
	found, _, path := g.RunDijstraFromNodes(node0, node6)
	assert.Equal(t, found, true)
	assert.Equal(t, len(path), 4)
	assert.Equal(t, convertPathToString(path), "0>2>7>3>6")
}

func convertPathToString(path []*Edge) string {
	toReturn := fmt.Sprintf("%v", path[0].GetStart().GetId())
	for _, e := range path {
		toReturn += fmt.Sprintf(">%v", e.GetEnd().GetId())
	}
	return toReturn
}
