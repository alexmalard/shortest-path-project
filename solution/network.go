package solution

import "fmt"

// Graph Structure also containing link between nodes and geolocalizable to check geolocalizable existency in graph easier
// We assume here that all nodes in graph are geolocalized
type Network struct {
	*Graph
	geolocalizables map[string]int // key is geolocalizable id, value is node id
}

type Geolocalizable struct {
	id                  string
	longitude, latitude float64
}

func MakeGeolocalizable(longitude, latitude float64) *Geolocalizable {
	id := generateIdFromPosition(longitude, latitude)
	return &Geolocalizable{id, longitude, latitude}
}

func generateIdFromPosition(longitude, latitude float64) string {
	return fmt.Sprintf("%v_%v", longitude, latitude)
}

func MakeNetwork() *Network {
	return &Network{
		MakeGraph(),
		make(map[string]int),
	}
}

// Method finding returning corresponding node for a given geolocalizable
func (n *Network) GetNodeFromPosition(longitude, latitude float64) (*Node, bool) {
	id := generateIdFromPosition(longitude, latitude)
	nodeId, foundGeoloc := n.geolocalizables[id]
	if !foundGeoloc {
		return nil, false
	}
	node, foundNode := n.GetNode(nodeId)
	return node, foundNode
}

// Method adding a node to the network
func (n *Network) AddNode(node *Node) {
	n.geolocalizables[node.position.id] = node.id
	n.Graph.AddNode(node)
}

// Function finding nodes in network for given coordinates
// Runs shortest path algorithm for given origin and destination
func (n *Network) RunDijkstraFromPositions(longStart, latStart, longEnd, latEnd float64) (bool, error, []*Edge) {
	fmt.Printf("\nFrom (%v,%v) to (%v,%v)", longStart, latStart, longEnd, latEnd)
	nodeStart, foundStart := n.GetNodeFromPosition(longStart, latStart)
	if !foundStart {
		return false, fmt.Errorf("Could not find node with coordinates (%v,%v)", longStart, latStart), nil
	}
	nodeEnd, foundEnd := n.GetNodeFromPosition(longEnd, latEnd)
	if !foundEnd {
		return false, fmt.Errorf("Could not find node with coordinates (%v,%v)", longEnd, latEnd), nil
	}
	found, totalLength, path := n.RunDijstraFromNodes(nodeStart, nodeEnd)
	fmt.Printf("\nFound : %v - Total length : %.2f", found, totalLength)
	return found, nil, path
}

// Return coordinates for a given geolocalized node
func (node *Node) GetLocation() (float64, float64) {
	return node.position.longitude, node.position.latitude
}
