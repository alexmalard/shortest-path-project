package solution

import "fmt"

// Methode in charge of building example graph
func BuildSimpleGraph() *Graph {
	g := MakeGraph()
	g.AddEdgeFromIds(4, 5, 0.35)
	g.AddEdgeFromIds(5, 4, 0.35)
	g.AddEdgeFromIds(4, 7, 0.37)
	g.AddEdgeFromIds(5, 7, 0.28)
	g.AddEdgeFromIds(7, 5, 0.28)
	g.AddEdgeFromIds(5, 1, 0.32)
	g.AddEdgeFromIds(0, 4, 0.38)
	g.AddEdgeFromIds(0, 2, 0.26)
	g.AddEdgeFromIds(7, 3, 0.39)
	g.AddEdgeFromIds(1, 3, 0.29)
	g.AddEdgeFromIds(2, 7, 0.34)
	g.AddEdgeFromIds(6, 2, 0.4)
	g.AddEdgeFromIds(3, 6, 0.52)
	g.AddEdgeFromIds(6, 0, 0.58)
	g.AddEdgeFromIds(6, 4, 0.93)

	return g
}

// Simple method printing path information
func PrintPath(path []*Edge) {
	for _, e := range path {
		fmt.Printf("\n %v %v %v", e.GetStart().GetId(), e.GetEnd().GetId(), e.GetWeight())
	}
}
