package solution

import (
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"
)

// Method reading nodes file and adding the corresponding geolocalized nodes to the network
func readGraphNodes(n *Network) {
	csvfile, err := os.Open("data/nodes.csv")
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(csvfile)
	//r := csv.NewReader(bufio.NewReader(csvfile))
	r.Read()
	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		idString := record[0]
		xString := record[1]
		yString := record[2]
		id, _ := strconv.Atoi(idString)
		x, _ := strconv.ParseFloat(xString, 64)
		y, _ := strconv.ParseFloat(yString, 64)
		long := x / 1e6
		lat := y / 1e6
		newNode := MakeNodeWithGeolocalizable(id, long, lat)
		n.AddNode(newNode)
	}
}

// Method reading edges file and adding the corresponding geolocalized edges to the network
func readGraphEdges(n *Network) {

	csvfile, err := os.Open("data/edges.csv")
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	// Parse the file
	r := csv.NewReader(csvfile)
	//r := csv.NewReader(bufio.NewReader(csvfile))
	r.Read()
	// Iterate through the records
	for {
		// Read each record from csv
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		startIdString := record[0]
		endIdString := record[1]
		weightStr := record[2]
		startId, _ := strconv.Atoi(startIdString)
		endId, _ := strconv.Atoi(endIdString)
		weight, _ := strconv.ParseFloat(weightStr, 64)

		n.AddEdgeFromIds(startId, endId, weight)
	}
}

// Method in charge of parsing inputs data and generating the corresponding network
func MakeNYCNetwork() *Network {
	network := MakeNetwork()
	readGraphNodes(network)
	readGraphEdges(network)
	return network
}
