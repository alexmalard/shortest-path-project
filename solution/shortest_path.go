package solution

// Element implementing
type DijkstraNodeElement struct {
	*Node
	value        float64
	index        int
	previousEdge *Edge
}

func MakeDijkstraNodeElement(node *Node, value float64, previousEdge *Edge) *DijkstraNodeElement {
	return &DijkstraNodeElement{node, value, 0, previousEdge}
}

func (n *DijkstraNodeElement) GetIndex() int {
	return n.index
}

func (n *DijkstraNodeElement) SetIndex(index int) {
	n.index = index
}

// Method in charge of comparing two elements in Priority Queue
func (n *DijkstraNodeElement) Compare(other PriorityQueueElement) int {
	otherNode := other.(*DijkstraNodeElement)
	if n.value > otherNode.value {
		return 1
	}
	if n.value == otherNode.value {
		return 0
	}
	return -1
}

// Dijkstra Algorithm computing shortest path from given source to destination
// Returns the incoming edge in the minimum weighted tree for every reached node
func (graph *Graph) Dijkstra(source, destination *Node) (map[int]*Edge, bool) {
	reachedNodes := make(map[int]*Edge) // key is NodeId, value is previousEdge in shortest path
	queue := MakePriorityQueue()
	sourcePQ := MakeDijkstraNodeElement(source, 0.0, nil)
	queue.Push(sourcePQ)
	reachedNodes[source.GetId()] = nil
	for len(queue.GetQueue()) > 0 {
		currentMinHeapNode, _ := queue.Pop()
		currentNode := currentMinHeapNode.(*DijkstraNodeElement)
		currentValue := currentNode.value
		previousEdge := currentNode.previousEdge

		if previousEdge != nil {
			reachedNodes[currentNode.GetId()] = previousEdge
		}

		if currentNode.GetId() == destination.GetId() {
			// We found the destination we wanted
			return reachedNodes, true
		}
		for _, nextEdge := range currentNode.GetOutgoingEdges() {
			nextNode := nextEdge.GetEnd()
			if _, found := reachedNodes[nextNode.GetId()]; found {
				// the ending node is already reached
				continue
			}
			nextValue := currentValue + nextEdge.GetWeight()
			if existingNodeInPQ, hasNodeInPQ := queue.GetElement(nextNode.GetId()); hasNodeInPQ {
				existingDNE, _ := existingNodeInPQ.(*DijkstraNodeElement)
				if nextValue >= existingDNE.value {
					// the ending node is already in queue with better value, no need to add it again
					continue
				}
			}
			nextNodePQ := MakeDijkstraNodeElement(nextNode, nextValue, nextEdge)
			queue.Push(nextNodePQ)
		}
	}
	return reachedNodes, false
}

// Calls Dijkstra'a algorithm a for a given source and destination
// Generates the corresponding path if found from shortest tree
func (graph *Graph) RunDijstraFromNodes(source, destination *Node) (bool, float64, []*Edge) {
	reachedNodes, found := graph.Dijkstra(source, destination)
	if !found {
		return false, 0.0, nil
	}
	totalLength, path := generatePath(source, destination, reachedNodes)
	return true, totalLength, path
}

// Generate the path from min wighted tree
// Also returns total path distance
func generatePath(source, destination *Node, reachedNodes map[int]*Edge) (float64, []*Edge) {
	toReturn := make([]*Edge, 0)
	totalLength := 0.0
	currentNode := destination
	for {
		previousEdge, _ := reachedNodes[currentNode.GetId()]
		if previousEdge == nil {
			break
		}
		totalLength += previousEdge.weight
		toReturn = append([]*Edge{previousEdge}, toReturn...)
		currentNode = previousEdge.GetStart()
	}
	return totalLength, toReturn
}
