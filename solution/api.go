package solution

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	geojson "github.com/paulmach/go.geojson"
)

var NYCNetwork *Network
var SimpleGraph *Graph

// Simple homePage function
func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "HomePage Endpoint Hit")
}

// Function hanling all the API requests
func HandleAPIRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/shortest-path/simpleGraph", RunDijkstraOnSimpleGraph).Methods("GET")
	myRouter.HandleFunc("/shortest-path/nycRequest/fromCoords={origin_longitude},{origin_latitude}/toCoords={destination_longitude},{destination_latitude}", getShortestPath).Methods("GET")
	log.Fatal(http.ListenAndServe(":8081", myRouter))
}

// Function in charge of calling shortest path algorithm for 2 geolocalized nodes given as arguments
// Return geojson object if the path is found, error message otherwise
func getShortestPath(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	start_lon_str := vars["origin_longitude"]
	start_lat_str := vars["origin_latitude"]
	end_lon_str := vars["destination_longitude"]
	end_lat_str := vars["destination_latitude"]

	fmt.Printf("\n%v %v %v %v", start_lon_str, start_lat_str, end_lon_str, end_lat_str)

	startLong, _ := strconv.ParseFloat(start_lon_str, 64)
	startLat, _ := strconv.ParseFloat(start_lat_str, 64)
	endLong, _ := strconv.ParseFloat(end_lon_str, 64)
	endLat, _ := strconv.ParseFloat(end_lat_str, 64)

	foundPath, err, path := NYCNetwork.RunDijkstraFromPositions(startLong, startLat, endLong, endLat)

	if err != nil {
		fmt.Fprintf(w, err.Error())
		return
	}

	if foundPath {
		lineString := generateLineStringFromPath(path)
		fc := geojson.NewFeatureCollection()
		fc.AddFeature(geojson.NewLineStringFeature(lineString))

		json.NewEncoder(w).Encode(fc)
	} else {
		fmt.Fprintf(w, "No path found from (%v,%v) to (%v,%v)", startLong, startLat, endLong, endLat)
	}
}

// Converting path into matrix for LineString object
func generateLineStringFromPath(path []*Edge) [][]float64 {
	toReturn := make([][]float64, len(path)+1)
	lon, lat := path[0].GetStart().GetLocation()
	toReturn[0] = []float64{lon, lat}
	for i, e := range path {
		lon, lat = e.GetEnd().GetLocation()
		toReturn[i+1] = []float64{lon, lat}
	}
	return toReturn
}

// Simple Method running shortest path algorithm for first example in test
// Returns path information if found, error message otherwise
func RunDijkstraOnSimpleGraph(w http.ResponseWriter, r *http.Request) {

	node0, _ := SimpleGraph.GetNode(0)
	node6, _ := SimpleGraph.GetNode(6)
	foundPath, totalLength, path := SimpleGraph.RunDijstraFromNodes(node0, node6)
	fmt.Fprintf(w, "Found path : %v", foundPath)
	if foundPath {
		fmt.Fprintf(w, "\nNb Edges : %v", len(path))
		for _, e := range path {
			fmt.Fprintf(w, "\n %v %v %v", e.GetStart().GetId(), e.GetEnd().GetId(), e.GetWeight())
		}
		fmt.Fprintf(w, "\nTotal length : %.2f", totalLength)
	}
}
