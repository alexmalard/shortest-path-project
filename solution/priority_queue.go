package solution

// Priority Queue interface contract
// Any element in the Priority Queue must satisfy this contract (implement the corresponding functions)
type PriorityQueueElement interface {
	Compare(other PriorityQueueElement) int
	GetId() int
	SetIndex(int)
	GetIndex() int
}

// Contains an array of element
// Also contains the map of present elements, to check element existency easily
type PriorityQueue struct {
	queue    []PriorityQueueElement
	elements map[int]PriorityQueueElement // key is id, value is element in queue
}

func MakePriorityQueue() *PriorityQueue {
	return &PriorityQueue{
		queue:    make([]PriorityQueueElement, 0),
		elements: make(map[int]PriorityQueueElement),
	}
}

func (h *PriorityQueue) GetQueue() []PriorityQueueElement {
	return h.queue
}

func (h *PriorityQueue) GetElement(id int) (PriorityQueueElement, bool) {
	existingElement, hasElement := h.elements[id]
	return existingElement, hasElement
}

func isAtRightIndex(v, other PriorityQueueElement) bool {
	if v.Compare(other) <= 0 {
		return true
	}
	return false
}

func (h *PriorityQueue) Push(v PriorityQueueElement) {
	if existingElement, hasElement := h.GetElement(v.GetId()); hasElement {
		h.update(existingElement, v)
	} else {
		h.queue = append(h.queue, v)
		v.SetIndex(len(h.queue) - 1)
		h.Up(len(h.queue) - 1)
	}
	h.elements[v.GetId()] = v
}

// update modifies the priority and value of an Item in the queue.
func (h *PriorityQueue) update(existingElement, newElement PriorityQueueElement) {
	index := existingElement.GetIndex()
	newElement.SetIndex(index)
	h.queue[index] = newElement
	if newElement.Compare(existingElement) < 0 {
		// we get a better value so we potentially need to move the element up
		h.Up(index)
	} else {
		// if it is worse, then we just need to ensure that the bottom part staisfies the heap properties, otherwise fix it
		h.Heapify(index)
	}
}

func (h *PriorityQueue) Up(currentIndex int) {
	for currentIndex > 0 {
		current := h.queue[currentIndex]
		parentIndex := getParent(currentIndex)
		parent := h.queue[parentIndex]
		if isAtRightIndex(parent, current) {
			break
		}
		// we swap current with parent
		h.Swap(currentIndex, parentIndex)
		currentIndex = parentIndex
	}
}

// Method to get parent index for a given one
func getParent(currentIndex int) int {
	isLeft := currentIndex%2 == 1
	if isLeft {
		return currentIndex / 2
	}
	return currentIndex/2 - 1
}

// Method in charge of fixing heap properties if needed from a given index
func (h *PriorityQueue) Heapify(index int) {
	currentElement := h.queue[index]
	leftChild, leftChildIndex, foundLeft := h.GetLeft(index)
	rightChild, rightChildIndex, foundRight := h.GetRight(index)
	childIndexToMoveUp := index
	childToMoveUp := currentElement
	if foundLeft && !isAtRightIndex(currentElement, leftChild) {
		childIndexToMoveUp = leftChildIndex
		childToMoveUp = h.queue[childIndexToMoveUp]
	}
	if foundRight && !isAtRightIndex(childToMoveUp, rightChild) {
		childIndexToMoveUp = rightChildIndex
	}
	if childIndexToMoveUp != index {
		// we detected a swap to do
		h.Swap(index, childIndexToMoveUp)
		h.Heapify(childIndexToMoveUp)
	}
}

func (h *PriorityQueue) Swap(i, j int) {
	h.queue[i], h.queue[j] = h.queue[j], h.queue[i]
	h.queue[i].SetIndex(i)
	h.queue[j].SetIndex(j)
}

func (h *PriorityQueue) Pop() (PriorityQueueElement, bool) {
	if len(h.queue) == 0 {
		return nil, false
	}
	// we get the root element and remove it from PriorityQueue
	toReturn := h.queue[0]
	h.queue = h.queue[1:]
	delete(h.elements, toReturn.GetId())
	if len(h.queue) > 1 {
		// move the last element at first position
		lastElement := h.queue[len(h.queue)-1:]
		lastElement[0].SetIndex(0)
		h.queue = append(lastElement, h.queue[:len(h.queue)-1]...)
		// reorganize the structure to maintain the PriorityQueue properties (=> Heapify)
		h.Heapify(0)
	}
	return toReturn, true
}

// Get left child if there exists
func (PriorityQueue *PriorityQueue) GetLeft(indexVertex int) (PriorityQueueElement, int, bool) {
	leftIndex := 2*indexVertex + 1
	if leftIndex < len(PriorityQueue.queue) {
		leftChild := PriorityQueue.queue[leftIndex]
		return leftChild, leftIndex, true
	}
	return nil, -1, false
}

// Get right child if there exists
func (PriorityQueue *PriorityQueue) GetRight(indexVertex int) (PriorityQueueElement, int, bool) {
	rightIndex := 2*indexVertex + 2
	if rightIndex < len(PriorityQueue.queue) {
		rightChild := PriorityQueue.queue[rightIndex]
		return rightChild, rightIndex, true
	}
	return nil, -1, false
}
