package solution

import (
	"strconv"
)

// This file contains all functions to build, update and get information in the graph data structure

// Simple graph data structure containing all nodes and edges
type Graph struct {
	nodes map[int]*Node // key is NodeId
	edges map[int]*Edge // key is EdgeId
}

type Node struct {
	id            int
	outgoingEdges map[int]*Edge // key is nodeEndId
	position      *Geolocalizable
}

type Edge struct {
	id         int
	name       string
	weight     float64
	start, end *Node
}

func MakeGraph() *Graph {
	return &Graph{
		nodes: make(map[int]*Node),
		edges: make(map[int]*Edge),
	}
}

func (g *Graph) GetNodes() map[int]*Node {
	return g.nodes
}

func (g *Graph) GetEdges() map[int]*Edge {
	return g.edges
}

func (g *Graph) GetNode(id int) (*Node, bool) {
	v, found := g.nodes[id]
	return v, found
}

func (g *Graph) GetEdgeFromId(id int) (*Edge, bool) {
	v, found := g.edges[id]
	return v, found
}

func (g *Graph) GetEdgeFromNodes(nodeStart, nodeEnd *Node) (*Edge, bool) {
	from, foundFrom := g.nodes[nodeStart.GetId()]
	if !foundFrom {
		return nil, false
	}
	edge, foundEdge := from.GetOutgoingEdge(nodeEnd)
	return edge, foundEdge
}

func (g *Graph) AddEdge(edge *Edge) bool {
	_, found := g.edges[edge.GetId()]
	if !found {
		g.edges[edge.GetId()] = edge
		return true
	}
	return false
}

func (g *Graph) AddNode(node *Node) bool {
	_, found := g.nodes[node.GetId()]
	if !found {
		g.nodes[node.GetId()] = node
		return true
	}
	return false
}

func (g *Graph) GetFollowingEdges(node *Node) map[int]*Edge {
	return node.GetOutgoingEdges()
}

func (n *Node) GetId() int {
	return n.id
}

func (n *Node) GetOutgoingEdges() map[int]*Edge {
	return n.outgoingEdges
}

func (n *Node) GetOutgoingEdge(to *Node) (*Edge, bool) {
	edge, found := n.outgoingEdges[to.GetId()]
	return edge, found
}

func (n *Node) AddOutgoingEdge(edge *Edge) bool {
	if _, found := n.outgoingEdges[edge.GetEnd().GetId()]; !found {
		n.outgoingEdges[edge.GetEnd().GetId()] = edge
		return true
	}
	return false
}

func (se *Edge) GetId() int {
	return se.id
}

func (se *Edge) GetName() string {
	return se.name
}

func (se *Edge) GetWeight() float64 {
	return se.weight
}

func (se *Edge) GetStart() *Node {
	return se.start
}

func (se *Edge) GetEnd() *Node {
	return se.end
}

func MakeEdge(id int, start, end *Node, weight float64) *Edge {
	name := generateEdgeNameFromNodes(start, end)
	return &Edge{
		id:     id,
		name:   name,
		weight: weight,
		start:  start,
		end:    end,
	}
}

func MakeNodeWithGeolocalizable(id int, long, lat float64) *Node {
	return &Node{
		id:            id,
		outgoingEdges: make(map[int]*Edge),
		position:      MakeGeolocalizable(long, lat),
	}
}

func MakeNode(id int) *Node {
	return &Node{
		id:            id,
		outgoingEdges: make(map[int]*Edge),
	}
}

func generateEdgeNameFromNodes(nodeStart, nodeEnd *Node) string {
	return strconv.Itoa(nodeStart.GetId()) + ">" + strconv.Itoa(nodeEnd.GetId())
}

func (g *Graph) addEdge(nodeStart, nodeEnd *Node, weight float64) bool {
	if _, found := g.GetEdgeFromNodes(nodeStart, nodeStart); found {
		return false
	}
	idEdge := len(g.edges)
	newEdge := MakeEdge(idEdge, nodeStart, nodeEnd, weight)
	g.AddEdge(newEdge)
	nodeStart.AddOutgoingEdge(newEdge)
	return true
}

func (g *Graph) addNodeFromId(id int) (*Node, bool) {
	if n, found := g.GetNode(id); found {
		return n, false
	}
	newNode := MakeNode(id)
	g.AddNode(newNode)
	return newNode, true
}

func (g *Graph) AddEdgeFromIds(nodeStartId, nodeEndId int, weight float64) bool {
	nodeStart, _ := g.addNodeFromId(nodeStartId)
	nodeEnd, _ := g.addNodeFromId(nodeEndId)
	return g.addEdge(nodeStart, nodeEnd, weight)
}
