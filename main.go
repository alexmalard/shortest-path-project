package main

import (
	. "gitlab.com/alexmalard/shortest-path-project/solution"
)

//Main function launcing the program and API Handling Requests
func main() {
	NYCNetwork = MakeNYCNetwork()
	SimpleGraph = BuildSimpleGraph()
	HandleAPIRequests()
}
