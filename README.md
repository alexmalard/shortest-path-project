# shortest-path-project

This is a repository for the shortest path distance problem

Data folder contains the edges and nodes modelling NYC infrastructure

Solution folder contains all files containing the solutions for: 
    1) Implement a shortest path algorithm
    2) DIMACS Challenge


To launch the program :

    -   either clone the repository into an IDE (VSCode e.g) with Go pluging installed and run main.go from there
    -   or download and install Golang directly and execute the following commands from the root folder in the repostory

Option 1 :

    Execute : go build (it will create an executable file that you can then run)

Option 2 :

    Execute : go run main.go (it will just launch the program)

Once the program is launched, you can either :

1) Run shortest path algorithm for first example (from source node 0 to destination 6)

    http://localhost:8081/shortest-path/simpleGraph

2) Run shortest path algorithm for NYC example (where you can specify the coordinates in the url)

Here is an example : from (-74.108786,40.840999) to (-74.165788,40.866199):

    http://localhost:8081/shortest-path/nycRequest/fromCoords=-74.108786,40.840999/toCoords=-74.165788,40.866199

    You can then visualize the results by copy pasting the geojson object in : https://geojson.io/


In case of issues, an executable file is already generated that can be launched