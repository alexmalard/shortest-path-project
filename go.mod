module gitlab.com/alexmalard/shortest-path-project

go 1.15

require (
	github.com/google/go-cmp v0.5.6 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/paulmach/go.geojson v1.4.0
	github.com/pkg/errors v0.9.1 // indirect
	gotest.tools v2.2.0+incompatible
)
